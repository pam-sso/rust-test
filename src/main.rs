use reqwest::blocking::Client;
use serde_json::{from_str as json_from_str, Value};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let client = Client::new();
    let res = client
        .post("http://httpbin.org/post")
        .body("eyJjb250ZW50IjogImFhYWEifQo=")
        .send()?;
    let json: Value = json_from_str(&res.text()?)?;
    let coded = json.get("data").unwrap().as_str().unwrap();
    println!("-> The post body (B64) is : {}", coded);
    let decoded = String::from_utf8(base64::decode(coded).unwrap())?;
    println!("-> The post body (str) is : {}", decoded.trim());
    let json2: Value = json_from_str(&decoded)?;
    let result = json2.get("content").unwrap().as_str().unwrap();
    println!("-> The result (str) is : {}", result);
    Ok(())
}
